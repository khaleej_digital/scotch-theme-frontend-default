require([
    'jquery'
], function($) {

    $(document).on('click', '.swatch-option',  function() {

        console.log("swatch option clicked");

        if($(this).hasClass('disabled')) {

            console.log("hasClass : DISABLED");

            $(this).addClass('selected');
            $('.action.tocart').hide();
            $('.notify-instock').show();
            $(this).siblings().removeClass('selected')
        } else {

            console.log("NOT hasClass : DISABLED");

            $(this).siblings('disabled').removeClass('selected');
            $('.action.tocart').show();
            $('.notify-instock').hide();
            if ($('.notify-instock__first-step .action.notify').css('display') == 'none') {
                $('.notify-instock__first-step .action.notify').css('display', '');
                $('.notify-instock__second-step').hide();
                $('.product-addto-links').insertAfter('.action.tocart');
            }
        }
    });

    $('.notify-instock__first-step .action.notify').on('click', function(e) {
        $(this).hide();
        $('.notify-instock__second-step').show();
        $('.product-addto-links').appendTo('.notify-instock .actions')
    });

    $('#notify-newsletter').on('change', function() {
        $(this).is(':checked') ?
        $('.newsletter-type').show() : $('.newsletter-type').hide();
    });


     $('.actions .action.notify').on('click', function(e) {

        console.log("add email to bdd && send notify");



    });
 

});
