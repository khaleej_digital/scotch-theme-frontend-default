define([
    "jquery"
], function ($) {
    'use strict';
    function filterUIEvents() {
        $(window).on("resize", function () {
            isMobile();
        });
        isMobile();
        function isMobile() {
            if($(window).innerWidth() <= 767){
                return true;
            } else {
                return false;
            }
        }
        $(document).on('click', '.filter-toggle', function () {
            if($(this).find('.more-filters').hasClass('more-hidden')){
                $(this).find('.more-filters').removeClass('more-hidden');
                $(this).find('.less-filters').addClass('more-hidden');
                $('.js-filter-hidden').addClass('filter-hide');
            } else {
                $(this).find('.more-filters').addClass('more-hidden');
                $(this).find('.less-filters').removeClass('more-hidden');
                $('.js-filter-hidden').removeClass('filter-hide');
            }
        });
        $(document).on('click', '.filter-options-title', function (e) {
            if($(this).parent('.filter-options').hasClass('select-open')){
                $('.block-filter-holder .filter-options.select-open').removeClass("select-open");
                e.preventDefault();
            } else {
                $('.block-filter-holder .filter-options.select-open').removeClass("select-open");
                $(this).parent(".filter-options").addClass("select-open");
                e.preventDefault();
            }
        });
        if(isMobile()){
            $("#maincontent").addClass("mobile-frame");
            $(document).on('click','.block-filter-holder .filter-title', function () {
                $('#maincontent').addClass("open-mobile-filter");
            });
            $(document).on('click','.block-filter-holder .close-btn', function () {
                $('#maincontent').removeClass("open-mobile-filter");
            });
        } else {
            $('#maincontent').removeClass("open-mobile-filter");
        }
    }
    var amShopbyWidgetMixin = {

        callAjax: function (clearUrl, data, pushState, cacheKey, isSorting) {
            var self = this;
            if (pushState || isSorting) {
                this.$shopbyOverlay.show();
            }

            data.every(function (item, key) {
                if (item.name.indexOf('[cat]') != -1) {
                    if (item.value == self.options.currentCategoryId) {
                        data.splice(key, 1);
                    } else {
                        item.value.split(',').filter(function (element) {
                            return element != self.options.currentCategoryId
                        }).join(',');
                    }

                    return false;
                }
            });
            data.push({name: 'shopbyAjax', value: 1});
            $.mage.amShopbyAjax.prototype.startAjax = true;

            if (!clearUrl) {
                clearUrl = self.options.clearUrl;
            }
            clearUrl = clearUrl.replace('amp;', '');
            self.clearUrl = clearUrl;

            return $.ajax({
                url: clearUrl,
                data: data,
                cache: true,
                success: function (response) {
                    try {
                        $.mage.amShopbyAjax.prototype.startAjax = false;

                        response = $.parseJSON(response);

                        if (response.isDisplayModePage) {
                            throw new Error();
                        }

                        if (cacheKey) {
                            self.cached[cacheKey] = response;
                        }

                        $.mage.amShopbyAjax.prototype.response = response;
                        if (response.newClearUrl
                            && (response.newClearUrl.indexOf('?p=') == -1 && response.newClearUrl.indexOf('&p=') == -1)) {
                            self.options.clearUrl = response.newClearUrl;
                        }

                        if (pushState || ($.mage.amShopbyApplyFilters && $.mage.amShopbyApplyFilters.prototype.showButtonClick) || isSorting) {
                            window.history.pushState({url: response.url}, '', response.url);
                        }

                        if (self.options.submitByClick !== 1 || isSorting) {
                            self.reloadHtml(response);
                        }

                        if ($.mage.amShopbyApplyFilters && $.mage.amShopbyApplyFilters.prototype.showButtonClick) {
                            $.mage.amShopbyApplyFilters.prototype.showButtonClick = false;
                            $.mage.amShopbyAjax.prototype.response = false;
                            self.reloadHtml(response);
                        }

                        if ($.mage.amShopbyApplyFilters) {
                            $.mage.amShopbyApplyFilters.prototype.showButtonCounter(response.productsCount);
                        }
                    } catch (e) {
                        var url = self.clearUrl ? self.clearUrl : self.options.clearUrl;
                        window.location = (this.url.indexOf('shopbyAjax') == -1) ? this.url : url;
                    }
                },
                error: function (response) {
                    try {
                        if (response.getAllResponseHeaders() != '') {
                            self.options.clearUrl ? window.location = self.options.clearUrl : location.reload();
                        }
                    } catch (e) {
                        window.location = (this.url.indexOf('shopbyAjax') == -1) ? this.url : self.options.clearUrl;
                    }
                },
                always: function () {
                    filterUIEvents();
                }
            });
        },
    };
    filterUIEvents();
    return function (targetWidget) {
        $.widget('custom.amShopbyAjax', targetWidget, amShopbyWidgetMixin);
        return $.custom.amShopbyAjax;
    };

});
