var config = {
    deps: [
        "js/main"
    ],
    config: {
        mixins: {
            'Amasty_Shopby/js/amShopbyAjax': {
                'Magento_LayeredNavigation/js/amShopbyAjax-mixin': true,
            },
            'Magento_Catalog/js/related-products': {
                'Magento_Catalog/js/related-products-mixin': true
            },
            'Magento_Catalog/js/upsell-products': {
                'Magento_Catalog/js/upsell-products-mixin': true
            },
            'Magento_Checkout/js/view/summary/abstract-total': {
                'Magento_Checkout/js/view/summary/abstract-total-mixin': true
            },
            'Magento_Checkout/js/view/shipping': {
                'Magento_Checkout/js/view/shipping-mixin': true
            }
        }
    }

};
