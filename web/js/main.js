define([
        'jquery',
        'matchMedia',
        'slick',
        'Magento_Customer/js/customer-data'
    ],
    function ($, mediaCheck, customerData) {
        "use strict";
        $(window).on("resize", function () {
            isTablet();
            isMobile();
            //tabsAccordeon();
        });

        isTablet();
        isMobile();
        //stickHeader();
        accordeonFooterMobile();

        //tabsAccordeon();
        function isMobile() {
            if($(window).innerWidth() <= 767){
                return true;
            } else {
                return false;
            }
        }
        function isTablet() {
            if($(window).innerWidth() <= 1024){
                return true;
            } else {
                return false;
            }
        }
        if(isMobile() != true){
            $('.header-wrapper-right').on('hover', function () {
                $('body').toggleClass('openMenu');
            });
        } else {
            $('body').removeClass('openMenu');
        }


        function accordeonFooterMobile() {
            $('.footer-links-collapsible li.parent >a').on("click", function (e) {
                if ($(this).next("ul.submenu").hasClass('open')) {
                    $(".footer-links-collapsible ul.submenu.open").removeClass("open");
                    $('.footer-links-collapsible li.parent >a.open').removeClass("open");
                    e.preventDefault();

                } else {
                    $(".footer-links-collapsible ul.submenu.open").removeClass("open");
                    $(this).next("ul").addClass("open");
                    $('.footer-links-collapsible li.parent >a.open').removeClass("open");
                    $(this).addClass("open");
                    e.preventDefault();
                }
            });
        }

        // nav toggle ---------
        $('.nav-toggle').appendTo('.column-first')

        // search toggle --------
        $('.block-search .block-title').on('click', function() {
            $(this).toggleClass('active');
            $(this).next('.block-content').toggleClass('active');
            $('.background-overlay').toggleClass('overlay');
            $('body').toggleClass('noscroll');
        });

        $('.field.search .action-close').on('click', function(e) {
            if ($('#search').attr('value').length) {
                $('#search').attr('value', '')
            }

            $('.block-search .block-title').removeClass('active');
            $('.block-search .block-content').removeClass('active');
            $('.background-overlay').removeClass('overlay');
            $('body').removeClass('noscroll');

            e.preventDefault();
        })

        $(document).mouseup(function (e) {
            let header = $('.header.content')
            if (!header.is(e.target) && header.has(e.target).length === 0 && $('.block-search .block-content').hasClass('active'))
                {
                    $('.background-overlay').removeClass('overlay');
                    $('body').removeClass('noscroll');
                    $('.block-search .block-content').removeClass('active');
                    $('.block-search .block-title').removeClass('active');
                }
         });

         // wishlist
         if($('.customer-welcome .link.wishlist').length) {
            $('.customer-welcome .link.wishlist').insertAfter('.minicart-wrapper')
        }

        // PLP - colour filter
        var colourOptions = $('.filter-options .macro_colour a')
        if (colourOptions.length) {
            $.each(colourOptions, function(i, item) {
               $(item).text($(item)[0].dataset.label)
             })
        }

        // My account - header - logged in

            mediaCheck({
                media: '(max-width: 767px)',
                entry: function() {
                    if ($('.header.links.Parent .customer-welcome').length) {
                        $('.header.links.Parent .customer-welcome').insertAfter('.link.wishlist')
                    }
                },
                exit: function() {
                    $('.column-third > .customer-welcome').insertBefore('.header.links.Parent .nav.item')
                }
            })


         mediaCheck({
            media: '(min-width: 768px)',
            entry: function() {
                $('.block-search').insertAfter('.header.links.Parent')
                if($(".account-nav-content ul.items").hasClass('slick-initialized')){
                    $(".account-nav-content ul.items").slick('unslick');
                }
            },
            exit: function() {
                $('.block-search').insertAfter('.nav-toggle');
                var currentIndex = $(".account-nav-content ul.items li.item.current").index();
                $(".account-nav-content ul.items").slick({
                    mobileFirst: true,
                    variableWidth: true,
                    swipeToSlide:true,
                    infinite:false,
                    initialSlide:currentIndex,
                    slidesToShow:2,
                    slidesToScroll: 1,
                    arrows: false,
                });
            }
        });
        customSelect();
        function customSelect(){
            $('select.custom-select').each(function(){
                if(!$(this).hasClass('initialized')){
                    var $this = $(this),
                        numberOfOptions = $(this).children('option').length;
                    $this.addClass('initialized');
                    $this.addClass('select-hidden');
                    $this.wrap('<div class="select"></div>');
                    $this.after('<div class="select-styled"></div>');

                    var $styledSelect = $this.next('div.select-styled');
                    $styledSelect.text($this.children('option:selected').eq(0).text());

                    var $list = $('<ul />', {
                        'class': 'select-options'
                    }).insertAfter($styledSelect);

                    for (var i = 0; i < numberOfOptions; i++) {
                        $('<li />', {
                            text: $this.children('option').eq(i).text(),
                            rel: $this.children('option').eq(i).val()
                        }).appendTo($list);
                    }

                    var $listItems = $list.children('li');
                    $list.addClass('select-hide');
                    $styledSelect.click(function(e) {
                        e.stopPropagation();
                        $('div.select-styled.active').not(this).each(function(){
                            $(this).removeClass('active').next('ul.select-options').addClass('select-hide');
                        });
                        $(this).toggleClass('active').next('ul.select-options').toggleClass('select-hide');
                    });

                    $listItems.click(function(e) {
                        e.stopPropagation();
                        $styledSelect.text($(this).text()).removeClass('active');
                        $this.val($(this).attr('rel'));
                        $this.change();
                        $list.addClass('select-hide');
                    });
                    $(document).click(function() {
                        $styledSelect.removeClass('active');
                        $list.addClass('select-hide');
                    });
                }
            });
        };

        mediaCheck({
            media: '(max-width: 767px)',
            entry: function() {
                $('.ves-megamenu.top-main-menu').prependTo($('.section-items.nav-sections-items'));
                //$('.ves-megamenu.help-menu').appendTo($('.section-items.nav-sections-items'));
                $('.ves-megamenu.help-menu').appendTo($('.sections.nav-sections'));

                $('.ves-megamenu.help-menu .level0 > a > .drill-opener').on('click', function () {
                    $('.action.nav-toggle').trigger('click');
                    setTimeout(function(){
                        $('.ves-megamenu.help-menu').addClass('help-menu-open');
                        $('.action.nav-toggle').trigger('click');
                    },500);
                });

                $('.ves-megamenu.help-menu .drilldown-back .drill-opener').on('click', function () {
                    $('.action.nav-toggle').trigger('click');
                    setTimeout(function(){
                        $('.ves-megamenu.help-menu').removeClass('help-menu-open');
                        $('.action.nav-toggle').trigger('click');
                    },500);
                });

                $('#login-link').prependTo($('.help-menu.ves-megamenu'));
                // $('.switcher-language').prependTo($('.help-menu.ves-megamenu'));
                 // $('.switcher-website').prependTo($('.help-menu.ves-megamenu'));

                $('.switcher-website .switcher-trigger').on('click touch', function() {
                    $('.switcher-options a').on('click touch', function() {
                        $('html').removeClass('nav-before-open nav-open')
                    })
                })
            },
            exit: function() {
                $('.nav-sections-items .ves-megamenu.top-main-menu').prependTo($('.header-content-column.column-first'));
                $('.nav-sections .ves-megamenu.help-menu').appendTo($('.header-content-column.column-third'));
                $('.block-search').insertAfter('.header .help-menu');
                // $('.switcher-website').appendTo($('.header-content-column.column-third'));
                 // $('.switcher-language').appendTo($('.header-content-column.column-third'));

            }
        });

        $(".block-products-list ol.product-items").slick({
            mobileFirst: true,
            slidesToShow:1,
            slidesToScroll: 1,
            arrows: false,
            rtl: window.location.pathname.includes('ar') ? true : false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        arrows: true
                    }
                }
            ]
        });
    });
