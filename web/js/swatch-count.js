/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'underscore',
    'mage/template',
    'mage/translate'
], function ($, _, mageTemplate, $t) {
    'use strict';

    $.widget('mage.SwatchCount', {
        options: {
            jsonConfig: {},
            coloursText: $t('colours'),
        },

        /**
         * @private
         */
        _init: function () {

            // Don't render the same set of swatches twice
            if ($(this.element).attr('data-rendered')) {
                return;
            }
            $(this.element).attr('data-rendered', true);

            this.options.jsonConfig.attributes = _.sortBy(this.options.jsonConfig.attributes, function (attribute) {
                return parseInt(attribute.position, 10);
            });

            if (this.options.jsonConfig !== '' && this.options.jsonSwatchConfig !== '') {

                var attr = $(this.options.jsonConfig.attributes);
                var count = 1;

                attr.each(function(index) {
                    if(attr[index].code === 'macro_colour') {
                        count = attr[index].options.length;
                    }
                });

                $(this.element).html('<span>'+count+' '+this.options.coloursText+'</span>');

                $(this.element).trigger('swatch.initialized');
            } else {
                console.log('SwatchRenderer: No input data received');
            }

            var targetEle = $(this.element).parent().find('.product.name.product-item-name');
            $(this.element).insertBefore(targetEle);

        }

    });

    return $.mage.SwatchCount;
});
